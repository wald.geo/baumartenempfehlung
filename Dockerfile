FROM node:16-alpine

RUN mkdir /app

COPY . /app

WORKDIR /app

RUN npm install

EXPOSE 3001
CMD ["node","editor.js"]
