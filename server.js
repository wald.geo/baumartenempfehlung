#!/bin/env node

const port = 3001;

const   express = require('express'),
        watch = require('node-watch'),
        // ol = require('ol'),
        bodyParser = require("body-parser"),
        app = express(),
        apiController = require('./controllers/apiController.js');

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.set('view engine', 'ejs');
    app.set('views', __dirname + '/views');

    // Adding static route
    app.use('/assets', express.static(__dirname + '/assets'))
    app.use('/views/assets', express.static(__dirname + '/views/assets'))

    app.get ('/', function(req, res) {
        res.render('index');
    });

    app.get ('/guide', function(req, res) {
        res.render('pages/index');
    });

    app.get('/editor/projektionswege', function(req, res) {
        res.sendFile(__dirname + '/editor/projektionswege.html');
    });

    app.get('/editor/waldgesellschaften', function(req, res) {
        res.sendFile(__dirname + '/editor/waldgesellschaften.html');
    });

    apiController(app);


    app.listen(port,()=>{
        console.log('Express server started. Served at http://localhost:' + port);
    });
